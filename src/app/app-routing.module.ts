import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(mod => mod.HomeModule)
  },
  {
    path: 'fob',
    loadChildren: () => import('./fob/fob.module').then(mod => mod.FobModule)
  },
  {
    path: 'efa',
    loadChildren: () => import('./efa/efa.module').then(mod => mod.EfaModule)
  },
  {
    path: 'bmw',
    loadChildren: () => import('./bmw/bmw.module').then(mod => mod.BmwModule)
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

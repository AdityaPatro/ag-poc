import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FobComponent } from './fob/fob.component';

const routes: Routes = [
  {
    path: '',
    component: FobComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FobRoutingModule { }

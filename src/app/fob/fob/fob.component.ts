import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fob',
  templateUrl: './fob.component.html',
  styleUrls: ['./fob.component.css']
})
export class FobComponent implements OnInit {
 title: string;
  constructor() { }

  ngOnInit() {
    this.title = 'FOB Page';
  }

}

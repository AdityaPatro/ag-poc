import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FobRoutingModule } from './fob-routing.module';
import { FobComponent } from './fob/fob.component';


@NgModule({
  declarations: [FobComponent],
  imports: [
    CommonModule,
    FobRoutingModule
  ]
})
export class FobModule { }

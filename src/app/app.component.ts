import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showHeader: boolean ;
  ngOnInit() {
    if (this.router.url === '/auth') {
      this.showHeader = false;
    } else {
      this.showHeader = true;
    }
  }

  constructor(private router: Router) { }


}



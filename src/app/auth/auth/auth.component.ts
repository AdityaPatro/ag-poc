import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  email: string;
  password: string;
  constructor(private router: Router) { }

  ngOnInit() {
    this.email = 'suku@gmail.com';
    this.password = '1234';
    console.log(this.router.url);
  }

  onLogin() {
    this.router.navigate(['/', 'home']);
  }

}

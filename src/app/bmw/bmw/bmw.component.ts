import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bmw',
  templateUrl: './bmw.component.html',
  styleUrls: ['./bmw.component.css']
})
export class BmwComponent implements OnInit {
title: string;
  constructor() { }

  ngOnInit() {
    this.title = 'BMW Page';
  }

}

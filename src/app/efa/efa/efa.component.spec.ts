import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EfaComponent } from './efa.component';

describe('EfaComponent', () => {
  let component: EfaComponent;
  let fixture: ComponentFixture<EfaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EfaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EfaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

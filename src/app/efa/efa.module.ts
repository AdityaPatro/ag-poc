import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EfaRoutingModule } from './efa-routing.module';
import { EfaComponent } from './efa/efa.component';


@NgModule({
  declarations: [EfaComponent],
  imports: [
    CommonModule,
    EfaRoutingModule
  ]
})
export class EfaModule { }

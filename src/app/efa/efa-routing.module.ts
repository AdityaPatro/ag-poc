import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EfaComponent } from './efa/efa.component';

const routes: Routes = [
  {
    path: '',
    component: EfaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EfaRoutingModule { }
